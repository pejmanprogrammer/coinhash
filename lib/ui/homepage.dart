

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hash_coin/const/colors.dart';
import 'package:hash_coin/controller/homepage.dart';
import 'package:hash_coin/model/payload.dart';
import 'package:hash_coin/ui/payloadrow.dart';
import 'package:hash_coin/utils/enums.dart';
import 'package:http/http.dart' as http;





class HomePage extends GetView<HomePageController>{

  HomePageController controller = Get.put(HomePageController());



  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: GetBuilder<HomePageController>(
          builder: (controller)
          {
             if(controller.stateData == StateData.Loading)
               return Container(color: new Color(0xFF736AB7),child: Center(child: CircularProgressIndicator(backgroundColor: Colors.yellow,)));
             else if(controller.stateData == StateData.NotConnected)
               {
                 return Container(child: Column(
                   children: [

                     IconButton(icon: Icon(Icons.refresh , color: Colors.black,), onPressed: pressedRefresh),
                     SizedBox(height: 10,),
                     Text("خطا در دسترسی به اینترنت"),

                   ],
                 ),);
               }
             else
               {
                 return Column(
                   children: <Widget>[

                     Expanded(
                       child: new Container(
                         color: new Color(0xFF736AB7),
                         child: new CustomScrollView(
                           scrollDirection: Axis.vertical,
                           shrinkWrap: false,
                           slivers: <Widget>[
                             new SliverPadding(
                               padding: const EdgeInsets.symmetric(vertical: 24.0),
                               sliver: new SliverList(
                                 delegate: new SliverChildBuilderDelegate(
                                       (context, index) => new PayloadRow(index: index),
                                   childCount: controller.payload.data?.length ?? 0,
                                 ),
                               ),
                             ),
                           ],
                         ),
                       ),
                     )


                   ],
                 );
               }

          },
        ));
  }

  void pressedRefresh() {

    controller.checkNetWork();

  }
}

/*Container(color: new Color(0xFF736AB7),child: Center(child: CircularProgressIndicator(backgroundColor: Colors.yellow,)))*/

/*FutureBuilder(
future: controller.callApi(),
builder: (context, AsyncSnapshot<Payload> snapshot) {
switch (snapshot.connectionState) {
case ConnectionState.none:
return Column(
children: [

IconButton(icon: Icon(Icons.refresh , color: backColor,), onPressed: pressedRefresh),
SizedBox(height: 8,),
Text("دسترسی شما دچار مشکل شده است"),

],
);
case ConnectionState.waiting:
return Container(color: new Color(0xFF736AB7),child: Center(child: CircularProgressIndicator(backgroundColor: Colors.yellow,)));
case ConnectionState.active:
return Text('');

case ConnectionState.done:
if (snapshot.hasError) {
return Text(
'${snapshot.error}',
style: TextStyle(color: Colors.red),
);
} else {

controller.payload = snapshot.data ;


return Column(
children: <Widget>[

Expanded(
child: new Container(
color: new Color(0xFF736AB7),
child: new CustomScrollView(
scrollDirection: Axis.vertical,
shrinkWrap: false,
slivers: <Widget>[
new SliverPadding(
padding: const EdgeInsets.symmetric(vertical: 24.0),
sliver: new SliverList(
delegate: new SliverChildBuilderDelegate(
(context, index) => new PayloadRow(index: index),
childCount: snapshot.data?.data?.length ?? 0,
),
),
),
],
),
),
)


],
);
}

}

return null ;

})*/


