

enum Name {
  ETHEREUM,
  TRON,
  OMNI,
  RSK_SMART_BITCOIN,
  BINANCE_COIN,
  STELLAR,
  NEO,
  V_SYSTEMS,
  ARDOR,
  QTUM,
  XRP,
  WAVES,
  ONTOLOGY,
  EOS,
  NEBULAS,
  NEM,
  BIT_SHARES,
  BITCOIN_CASH,
  INT_CHAIN,
  COSMOS,
  UBIQ,
  VE_CHAIN,
  NU_BITS,
  PIVX,
  COUNTERPARTY,
  KOMODO,
  ICON,
  GX_CHAIN,
  VITE,
  WANCHAIN,
  IOST,
  TRUE_CHAIN,
  ETHEREUM_CLASSIC
}

final nameValues = EnumValues({
  "Ardor": Name.ARDOR,
  "Binance Coin": Name.BINANCE_COIN,
  "Bitcoin Cash": Name.BITCOIN_CASH,
  "BitShares": Name.BIT_SHARES,
  "Cosmos": Name.COSMOS,
  "Counterparty": Name.COUNTERPARTY,
  "EOS": Name.EOS,
  "Ethereum": Name.ETHEREUM,
  "Ethereum Classic": Name.ETHEREUM_CLASSIC,
  "GXChain": Name.GX_CHAIN,
  "ICON": Name.ICON,
  "INT Chain": Name.INT_CHAIN,
  "IOST": Name.IOST,
  "Komodo": Name.KOMODO,
  "Nebulas": Name.NEBULAS,
  "NEM": Name.NEM,
  "Neo": Name.NEO,
  "NuBits": Name.NU_BITS,
  "Omni": Name.OMNI,
  "Ontology": Name.ONTOLOGY,
  "PIVX": Name.PIVX,
  "Qtum": Name.QTUM,
  "RSK Smart Bitcoin": Name.RSK_SMART_BITCOIN,
  "Stellar": Name.STELLAR,
  "TRON": Name.TRON,
  "TrueChain": Name.TRUE_CHAIN,
  "Ubiq": Name.UBIQ,
  "VeChain": Name.VE_CHAIN,
  "VITE": Name.VITE,
  "v.systems": Name.V_SYSTEMS,
  "Wanchain": Name.WANCHAIN,
  "Waves": Name.WAVES,
  "XRP": Name.XRP
});

enum Slug {
  ETHEREUM,
  TRON,
  OMNI,
  RSK_SMART_BITCOIN,
  BINANCE_COIN,
  STELLAR,
  NEO,
  V_SYSTEMS,
  ARDOR,
  QTUM,
  XRP,
  WAVES,
  ONTOLOGY,
  EOS,
  NEBULAS_TOKEN,
  NEM,
  BITSHARES,
  BITCOIN_CASH,
  INT_CHAIN,
  COSMOS,
  UBIQ,
  VECHAIN,
  NUBITS,
  PIVX,
  COUNTERPARTY,
  KOMODO,
  ICON,
  GXCHAIN,
  VITE,
  WANCHAIN,
  IOSTOKEN,
  TRUECHAIN,
  ETHEREUM_CLASSIC
}

final slugValues = EnumValues({
  "ardor": Slug.ARDOR,
  "binance-coin": Slug.BINANCE_COIN,
  "bitcoin-cash": Slug.BITCOIN_CASH,
  "bitshares": Slug.BITSHARES,
  "cosmos": Slug.COSMOS,
  "counterparty": Slug.COUNTERPARTY,
  "eos": Slug.EOS,
  "ethereum": Slug.ETHEREUM,
  "ethereum-classic": Slug.ETHEREUM_CLASSIC,
  "gxchain": Slug.GXCHAIN,
  "icon": Slug.ICON,
  "int-chain": Slug.INT_CHAIN,
  "iostoken": Slug.IOSTOKEN,
  "komodo": Slug.KOMODO,
  "nebulas-token": Slug.NEBULAS_TOKEN,
  "nem": Slug.NEM,
  "neo": Slug.NEO,
  "nubits": Slug.NUBITS,
  "omni": Slug.OMNI,
  "ontology": Slug.ONTOLOGY,
  "pivx": Slug.PIVX,
  "qtum": Slug.QTUM,
  "rsk-smart-bitcoin": Slug.RSK_SMART_BITCOIN,
  "stellar": Slug.STELLAR,
  "tron": Slug.TRON,
  "truechain": Slug.TRUECHAIN,
  "ubiq": Slug.UBIQ,
  "vechain": Slug.VECHAIN,
  "vite": Slug.VITE,
  "v-systems": Slug.V_SYSTEMS,
  "wanchain": Slug.WANCHAIN,
  "waves": Slug.WAVES,
  "xrp": Slug.XRP
});

enum Symbol {
  ETH,
  TRX,
  OMNI,
  RBTC,
  BNB,
  XLM,
  NEO,
  VSYS,
  ARDR,
  QTUM,
  XRP,
  WAVES,
  ONT,
  EOS,
  NAS,
  XEM,
  BTS,
  BCH,
  INT,
  ATOM,
  UBQ,
  VET,
  USNBT,
  PIVX,
  XCP,
  KMD,
  ICX,
  GXC,
  VITE,
  WAN,
  IOST,
  TRUE,
  ETC
}

final symbolValues = EnumValues({
  "ARDR": Symbol.ARDR,
  "ATOM": Symbol.ATOM,
  "BCH": Symbol.BCH,
  "BNB": Symbol.BNB,
  "BTS": Symbol.BTS,
  "EOS": Symbol.EOS,
  "ETC": Symbol.ETC,
  "ETH": Symbol.ETH,
  "GXC": Symbol.GXC,
  "ICX": Symbol.ICX,
  "INT": Symbol.INT,
  "IOST": Symbol.IOST,
  "KMD": Symbol.KMD,
  "NAS": Symbol.NAS,
  "NEO": Symbol.NEO,
  "OMNI": Symbol.OMNI,
  "ONT": Symbol.ONT,
  "PIVX": Symbol.PIVX,
  "QTUM": Symbol.QTUM,
  "RBTC": Symbol.RBTC,
  "TRUE": Symbol.TRUE,
  "TRX": Symbol.TRX,
  "UBQ": Symbol.UBQ,
  "USNBT": Symbol.USNBT,
  "VET": Symbol.VET,
  "VITE": Symbol.VITE,
  "VSYS": Symbol.VSYS,
  "WAN": Symbol.WAN,
  "WAVES": Symbol.WAVES,
  "XCP": Symbol.XCP,
  "XEM": Symbol.XEM,
  "XLM": Symbol.XLM,
  "XRP": Symbol.XRP
});


enum Tag { MINEABLE }

enum StateData
{
  Loading , Success , NotConnected
}



class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
