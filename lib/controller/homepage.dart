


import 'package:connectivity/connectivity.dart';
import 'package:get/get.dart';
import 'package:hash_coin/callApi/getData.dart';
import 'package:hash_coin/const/url.dart';
import 'package:hash_coin/model/payload.dart';
import 'package:hash_coin/utils/enums.dart';

class HomePageController extends GetxController
{

  Payload payload;
  StateData stateData  = StateData.Loading;




  callApi() async
  {

    stateData  = StateData.Loading ;
    update();
    payload = await getData(url: GetDataCoinUrl);
    stateData = StateData.Success ;
    update();

  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    checkNetWork();

  }

  checkNetWork() async
  {

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi) {
      callApi();
    } else if (connectivityResult == ConnectivityResult.none) {
      stateData = StateData.NotConnected ;
      update();
    }


  }
}